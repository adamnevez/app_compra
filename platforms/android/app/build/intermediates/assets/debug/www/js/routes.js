angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
      
        
    .state('menu', {
      url: '/page1',
      templateUrl: 'templates/menu.html',
      controller: 'menuCtrl'
    })
      
    
    .state('cart', {
      url: '/page2',
      templateUrl: 'templates/cart.html',
      controller: 'cartCtrl'
    })
      
    
    .state('checkOut', {
      url: '/page3',
      templateUrl: 'templates/checkOut.html',
      controller: 'checkOutCtrl'
    })
	
        
    .state('login', {
      url: '/page4',
      templateUrl: 'templates/login.html',
      controller: 'loginCtrl',
	  
		resolve:{
			   "check":function($location){  
				  if(sessionStorage.getItem('loggedin_id')){ 
            $location.path('/page9');
          }else{  
            $location.path('/page4');   
          }
	     }
	   }
    
    })
       
        
   .state('signup', {
      url: '/page5',
      templateUrl: 'templates/signup.html',
      controller: 'signupCtrl'
    })
        
      

    .state('filterBy', {
      url: '/page6',
      templateUrl: 'templates/filterBy.html',
      controller: 'filterByCtrl'
    })

          
        
    .state('sortBy', {
      url: '/page7',
      templateUrl: 'templates/sortBy.html',
      controller: 'sortByCtrl'
    })
        
        
    .state('payment', {
      url: '/page8',
      templateUrl: 'templates/payment.html',
      controller: 'paymentByCtrl'
    })
        
      
        
    .state('profile', {
      url: '/page9',
      templateUrl: 'templates/profile.html',
      controller: 'profileCtrl'  
    })
        
      
      
    .state('myOrders', {
      url: '/page10',
      templateUrl: 'templates/myOrders.html',
      controller: 'myOrdersCtrl'
    })
        
      

    .state('editProfile', {
      url: '/page11',
      templateUrl: 'templates/editProfile.html',
      controller: 'editProfileCtrl'
    })
        
        
    .state('favorates', {
      url: '/page12',
      templateUrl: 'templates/favorates.html',
      controller: 'favoratesCtrl'
    })
        
      
    .state('productPage', {
      url: '/page13',
      templateUrl: 'templates/productPage.html',
      controller: 'productPageCtrl'
    })


  // function of control login company 

    .state('loginEmpresa', {
      url: '/page14',
      templateUrl: 'templates/loginEmpresa.html',
      controller: 'loginEmpresaCtrl',
    
    resolve:{
      "check":function($location){  
        if(sessionStorage.getItem('loggedin_idEmpresa')) 
          $location.path('/page22');   
        else
          $location.path('/page14'); 
      }
    }
   
    })

    .state('singupEmpresa', {
      url: '/page15',
      templateUrl: 'templates/singupEmpresa.html',
      controller: 'signupEmpresaCtrl'
    
    })

    .state('pageMarket', {
      url: '/page16',
      templateUrl: 'templates/pageMarket.html',
      controller: 'pageMarketCtrl'
  })

    .state('marketCarrefour', {
      url: '/page17',
      templateUrl: 'templates/market/marketCarrefour.html',
      controller: 'marketCarrefourCtrl'
  })


  .state('marketDB', {
       url: '/page18',
       templateUrl: 'templates/market/marketDb.html',
       controller: 'marketDbCtrl'
   })

  .state('marketNovaera', {
       url: '/page19',
       templateUrl: 'templates/market/marketNovaera.html',
       controller: 'marketNovaeraCtrl'
   })
    
    .state('filterByCarrefour', {
      url: '/page18',
      templateUrl: 'templates/filterByCarrefour.html',
      controller: 'filterByCarrefourCtrl'
    })

    // .state('filterByDBsuper', {
    //   url: '/page19',
    //   templateUrl: 'templates/filterByDBsuper.html',
    //   controller: 'filterByDBsuperCtrl'
    // })

    // .state('filterByNovaera', {
    //   url: '/page20',
    //   templateUrl: 'templates/filterByNovaera.html',
    //   controller: 'filterByNovaeraCtrl'
    // })

     .state('paymentinCard', {
      url: '/page20',
      templateUrl: 'templates/paymentinCard.html',
      controller: 'paymentByCtrl'
    })

     .state('paymentOnline', {
      url: '/page21',
      templateUrl: 'templates/paymentOnline.html',
      controller: 'paymentByCtrl'
    })

    .state('profileEmpresa', {
      url: '/page22',
      templateUrl: 'templates/profileEmpresa.html',
      controller: 'profileEmpresaCtrl'  
    })

  $urlRouterProvider.otherwise('/page1');   // if none of the above states are matched, use this as the fallback

});