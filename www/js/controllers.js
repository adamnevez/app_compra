angular.module('app.controllers', ['ngResource'])

.controller('menuCtrl', function($scope,$http,$rootScope,$ionicLoading,$ionicPopup,sharedCartService,sharedFilterService,Menu) {
	$scope.page = 0;
	$rootScope.allItens;
	$scope.loading = false;
    	$rootScope.show = function() {
        $ionicLoading.show({
          template: '<p>Loading...</p><ion-spinner icon="android"></ion-spinner>'
        });
      };

      $rootScope.hide = function(){
        $ionicLoading.hide();
      }; 

	//put cart after menu
	var cart = sharedCartService.cart;
	
	$scope.slide_items=[    			
		{"p_image_id":"slide_1"},				
		{"p_image_id":"slide_2"},
		{"p_image_id":"slide_3"}  
	];

					   
	$scope.noMoreItemsAvailable = false; // lazy load list
	
  	//loads the menu----onload event
	$scope.$on('$stateChangeSuccess', function() {
		$scope.loadMore();  //Added Infine Scroll
	});
	
	// Loadmore() called inorder to load the list 
	$scope.loadMore = function() {
			$scope.loading = true;
			str = sharedFilterService.getUrl(0);
			$http.get(str).success(function (response){
				//.log(response['records']);
				$scope.loading = false;
				$scope.menu_items = response[0].records;
				$rootScope.allItens = response[0].records;
				$scope.hasmore=response[0].has_more;	//"has_more": 0	or number of items left
				$scope.$broadcast('scroll.infiniteScrollComplete');
			});	

		//more data can be loaded or not
		if ($scope.hasmore == 0 ) {
			 $scope.noMoreItemsAvailable = true;
		}
	};
	
	
 //show product page
	$scope.showProductInfo=function (id,desc,img,name,price) {	 
		 sessionStorage.setItem('product_info_id', id);
		 sessionStorage.setItem('product_info_desc', desc);
		 sessionStorage.setItem('product_info_img', img);
		 sessionStorage.setItem('product_info_name', name);
		 sessionStorage.setItem('product_info_price', price);
		 window.location.href = "#/page13";
	 };

//add to cart function
	 $scope.addToCart=function(id,image,name,price){ 
	 	var alertPopup = $ionicPopup.alert({
            title: 'Carrinho',
            template: 'item adicionado ao carrinho!'
        });
		cart.add(id, image, name, price, 1);	
	 };	 
})
   
.controller('cartCtrl', function($scope,sharedCartService,$ionicPopup,$state) {
		
	//onload event-- to set the values
		$scope.$on('$stateChangeSuccess', function () {
			$scope.cart = sharedCartService.cart;
			console.log($scope.cart);
			$scope.total_qty = sharedCartService.total_qty;
			$scope.total_amount = sharedCartService.total_amount;		
		});
		
	//remove function
		$scope.removeFromCart = function(c_id){
			$scope.cart.drop(c_id);	
			$scope.total_qty = sharedCartService.total_qty;
			$scope.total_amount = sharedCartService.total_amount;
			
		};
		
		$scope.inc = function(c_id){
			$scope.cart.increment(c_id);
			$scope.total_qty = sharedCartService.total_qty;
			$scope.total_amount = sharedCartService.total_amount;
		};
		
		$scope.dec = function(c_id){
			$scope.cart.decrement(c_id);
			$scope.total_qty = sharedCartService.total_qty;
			$scope.total_amount = sharedCartService.total_amount;
		};
		
		$scope.checkout = function(){
			if($scope.total_amount > 0)
				$state.go('checkOut');
			else{
				var alertPopup = $ionicPopup.alert({
					title: 'Nenhum item no seu carrinho',
					template: 'Por favor, adicione alguns itens!'
				});
			}
		};
})
   
.controller('checkOutCtrl', function($scope,$ionicPopup) {

	$scope.loggedin=function(){
		if(sessionStorage.getItem('loggedin_id') == null)
			 return 1; 
		else{
			$scope.loggedin_name = sessionStorage.getItem('loggedin_name');
			$scope.loggedin_id = sessionStorage.getItem('loggedin_id');
			$scope.loggedin_phone = sessionStorage.getItem('loggedin_phone');
			$scope.loggedin_address = sessionStorage.getItem('loggedin_address');
			$scope.loggedin_pincode = sessionStorage.getItem('loggedin_pincode');
			return 0;

		}
	};
	$scope.showPopup = function() {
    var alertPopup = $ionicPopup.alert({
      title: 'App Compras Informa!',
      template: 'Acima de 150$ Entrega Gratis'
    });
    alertPopup.then(function(res) {
    window.location.href = "#/page8";
    });
  };

})



.controller('indexCtrl', function($scope,sharedCartService) {
	//$scope.total = 10; 
})
   
.controller('loginCtrl', function($scope,$rootScope,$http,$ionicLoading,$ionicPopup,$state,$ionicHistory,Autenticacao) {

		$scope.login = function(data) {
			$scope.user = data
			$rootScope.show();
			$http.get(HOST_SERVER+`/user/login?email=${data.email}&password=${data.password}`,{ timeout: 30000 })
			.then(function(response) {

					$rootScope.hide();
					$scope.user_details = response.data;
					sessionStorage.setItem('loggedin_name', $scope.user_details.u_name);
					sessionStorage.setItem('loggedin_id', $scope.user_details.u_id );
					sessionStorage.setItem('loggedin_phone', $scope.user_details.u_phone);
				    sessionStorage.setItem('loggedin_address', $scope.user_details.u_address);
					sessionStorage.setItem('loggedin_pincode', $scope.user_details.u_pincode);
					
					$ionicHistory.nextViewOptions({
						disableAnimate: true,
						disableBack: true
					});
					lastView = $ionicHistory.backView();
					console.log('Last View',lastView);
					//BUG to be fixed soon
					/*if(lastView.stateId=="checkOut"){ $state.go('checkOut', {}, {location: "replace", reload: true}); }
					else{*/
			        	$state.go('profile', {}, {location: "replace", reload: true});
					//}

			},function(error){
				console.log(error);
					$scope.hide();
					var alertPopup = $ionicPopup.alert({
						title: 'Falha no Login!',
						template: 'Por favor, verifique suas credenciais!'
					});

			})

		};
		
})
   
.controller('signupCtrl', function($scope,$http,$rootScope,$ionicLoading,$ionicPopup,$state,$ionicHistory,Autenticacao) {

	$scope.signup = function(data){

		//$rootScope.show();
		$http.post(HOST_SERVER+"/user/register?nome=${data.name}&email=${data.email}&password=${data.password}&phone=${data.phone}&pin=${data.pincode}", 
			{ timeout: 30000 })
			.then(function(response) {
				console.log(response.data[0].result);
				//$rootScope.hide();
				if(response.data[0].result.created == "1"){
					$scope.title = "Conta criada!";
					$scope.template = "Sua conta foi criada com sucesso!";
						
					//no back option
					$ionicHistory.nextViewOptions({
						disableAnimate: true,
						disableBack: true
					});
					$state.go('login', {}, {location: "replace", reload: true});
					
				}else if(response.data[0].result.exists == "1"){
					$scope.title = "E-mail já existe";
					$scope.template = "Por favor, clique esqueceu a senha se necessário";
					
				}else{
					$scope.title = "Failed";
					$scope.template = "Entre em contato com nossa equipe técnica";
				}
					
				var alertPopup = $ionicPopup.alert({
						title: $scope.title,
						template: $scope.template
				});

			}, function(error) {
				//$rootScope.hide();	

			})
			
	}
})

// controllerLoginEmpresa 

 .controller('loginEmpresaCtrl', function($scope,$rootScope,$http,$ionicLoading,$ionicPopup,$state,$ionicHistory,AutenticacaoEmpresa) {

		$scope.user = {};
		$scope.loginEmpresa = function(data) {	
			//$rootScope.show();	
			AutenticacaoEmpresa.login.get(
				{
					cnpj:data.cnpj,
					password:data.password
				}, function(response){
					$scope.empresa_details = response;
					console.log($scope.empresa_details)
					sessionStorage.setItem('loggedin_nomeEmpresa', $scope.empresa_details.nome);
					sessionStorage.setItem('loggedin_cnpjEmpresa', $scope.empresa_details.cnpj);
					sessionStorage.setItem('loggedin_Senha', $scope.empresa_details.password);
					sessionStorage.setItem('loggedin_Nome', $scope.empresa_details.nome);
					sessionStorage.setItem('loggedin_emailEmpresa',$scope.empresa_details.email);
					$rootScope.hide();
					$ionicHistory.nextViewOptions({
						disableAnimate: true,
						disableBack: true
					});
					lastView = $ionicHistory.backView();
					console.log('Last View', lastView);
					$rootScope.hide();
					//BUG to be fixed soon
					/*if(lastView.stateId=="checkOut"){ $state.go('checkOut', {}, {location: "replace", reload: true}); }
					else{*/
			        	$state.go('profileEmpresa', {}, {location: "replace", reload: true});
					//}	
			},function(error){
				$rootScope.hide();
				var alertPopup = $ionicPopup.alert({
						title: 'Login failed!',
						template: 'Por favor, verifique suas credenciais!'
					});
			})
		}
})
.controller('signupEmpresaCtrl', function($scope,$rootScope,$http,$ionicPopup,$state,$ionicHistory) {

	$scope.signupEmpresa = function(data){
		$rootScope.show();
		$http.post(HOST_SERVER+"/empresa/register?nome=${data.nameEmpresa}&cnpj=${data.cnpjEmpresa}&password=${data.password}&email=${data.address}&pin=${data.pincode}",
			{ timeout: 30000 })
					.then(function(response){

					console.log(response);
						$rootScope.hide();
					if(response.data[0].created=="1"){
						$scope.title="Conta criada!";
						$scope.template="Sua conta foi criada com sucesso!";
						
					//no back option
						$ionicHistory.nextViewOptions({
							disableAnimate: true,
							disableBack: true
						});
					
					}else if(response.data[0].exists=="1"){
						$scope.title="E-mail já existe";
						$scope.template="Por favor, clique esqueceu a senha se necessário";
					
					}else{
						scope.title="Failed";
						$scope.template="Entre em contato com nossa equipe técnica";
					}
					
					var alertPopup = $ionicPopup.alert({
							title: $scope.title,
							template: $scope.template
					});
					$state.go('loginEmpresa', {}, {location: "replace", reload: true});

					},function(error){
						$rootScope.hide();
					})
			}
				
	})
 
  
.controller('filterByCtrl', function($scope,sharedFilterService) {

  $scope.Categories = [
    {id: 1, name: 'Material de limpeza'},
	{id: 2, name: 'Alimentícios'},
	{id: 3, name: 'Bebidas'},
	{id: 4, name: 'Higiene Pessoal'}
  ];
  
  $scope.getCategory = function(cat_list){
    categoryAdded = cat_list;
	var c_string = ""; 	// will hold the category as string
	
	for(var i=0; i < categoryAdded.length; i++)
		 c_string += (categoryAdded[i].id+"||"); 
	
	c_string = c_string.substr(0, c_string.length - 2);
	sharedFilterService.category = c_string;
	window.location.href = "#/page1";
  };

})

.controller('sortByCtrl', function($scope,sharedFilterService) {
	$scope.sort = function(sort_by){
		sharedFilterService.sort=sort_by;
		console.log('sort',sort_by);		
		window.location.href = "#/page1";
	};
})
   
//Payment in Card and Online
.controller('paymentByCtrl', function($scope,$http,$ionicPopup){
	$scope.url = function(card){
		if (card == 'c'){
			window.location.href = "#/page20";
		}else if(card == 'd'){
			window.location.href = "#/page21";
		}
	};
    $scope.showPopup = function() {

		 $http.get(HOST_SERVER+`/compra/finalizar`,{ timeout: 30000 })
			.then(function(response) {
				console.log(response);
				

			},function(error){
				

			})

    var alertPopup = $ionicPopup.alert({
      title: 'App Compras Informa!',
      template: 'Suas Compras Foram Efetuadas'
    });
    alertPopup.then(function(res) {
    });
  };
})
// .controller('paymentinCardCtrl', function($scope) {
// 	$scope.paymentinCard = function(){
// 		window.location.href ="#/page19";
// 	};
// })

// // .controller('paymentOnlineCtrl', function($scope) {
// // 	$scope.paymentOnline = function(){
// // 		window.location.href ="#/page21";
// // 	};
// // })				

// finally payment


.controller('pageMarketCtrl', function($scope) {
	$scope.marketf = function(marke){	
	console.log('entrei',marke);
		if (marke == '1'){
			window.location.href = "#/page17";
		}else if (marke == '2'){
			window.location.href = "#/page18";
		}else if(marke == '3'){
			window.location.href = "#/page19";
		}
	};
})

  

.controller('profileCtrl', function($scope,$rootScope,$ionicHistory,$state) {

		$scope.loggedin_name = sessionStorage.getItem('loggedin_name');
		$scope.loggedin_id = sessionStorage.getItem('loggedin_id');
		$scope.loggedin_phone = sessionStorage.getItem('loggedin_phone');
		$scope.loggedin_address = sessionStorage.getItem('loggedin_address');
		$scope.loggedin_pincode = sessionStorage.getItem('loggedin_pincode');
		
		$scope.logout=function(){
				delete sessionStorage.loggedin_name;
				delete sessionStorage.loggedin_id;
				delete sessionStorage.loggedin_phone;
				delete sessionStorage.loggedin_address;
				delete sessionStorage.loggedin_pincode;
				
				console.log('Logoutctrl',sessionStorage.getItem('loggedin_id'));
				
				$ionicHistory.nextViewOptions({
					disableAnimate: true,
					disableBack: true
				});
				$state.go('menu', {}, {location: "replace", reload: true});
		};
})
   

 
//editarcadastroCliente 
.controller('editProfileCtrl', function($scope) {
		$scope.loggedin_name = sessionStorage.getItem('loggedin_name');
		$scope.loggedin_phone = sessionStorage.getItem('loggedin_phone');
		$scope.loggedin_address = sessionStorage.getItem('loggedin_address');
	
	$scope.Logout=function(){
			delete sessionStorage.loggedin_name;
			delete sessionStorage.loggedin_phone;
			delete sessionStorage.loggedin_address;
				
				console.log('editprofilectrl ->',sessionStorage.getItem('loggedin_id'));
				
				$ionicHistory.nextViewOptions({
					disableAnimate: true,
					disableBack: true
				});
				$state.go('menu', {}, {location: "replace", reload: true});
		};
})
   

//ControllerMarketCarrefour ---------------------------------------------------------//

.controller('marketCarrefourCtrl', function($scope,$http,$rootScope,$ionicPopup,sharedCartService,sharedFilterService) {
	//put cart after menu
	var cart = sharedCartService.cart;

	$scope.slide_items=[    			
		{"p_image_id":"slide_1"},				
		{"p_image_id":"slide_2"},
		{"p_image_id":"slide_3"}  
	];					   
	$scope.noMoreItemsAvailable = false; // lazy load list

	$scope.$on('$stateChangeSuccess', function() {
		$scope.loadMore();  //Added Infine Scroll
	});
	
	$scope.loadMore = function() {
			$scope.loading = true;
			str = sharedFilterService.getUrl(1);
			$http.get(str).success(function (response){
				//.log(response['records']);
				$scope.loading = false;
				$scope.menu_items = response[0].records;
				$rootScope.allItens = response[0].records;
				$scope.hasmore=response[0].has_more;	//"has_more": 0	or number of items left
				$scope.$broadcast('scroll.infiniteScrollComplete');
			});	

	//more data can be loaded or not
		if ($scope.hasmore == 0 ) {
			 $scope.noMoreItemsAvailable = true;
		}
	};
 //show product page
	$scope.showProductInfo=function (id,desc,img,name,price) {	 
		 sessionStorage.setItem('product_info_id', id);
		 sessionStorage.setItem('product_info_desc', desc);
		 sessionStorage.setItem('product_info_img', img);
		 sessionStorage.setItem('product_info_name', name);
		 sessionStorage.setItem('product_info_price', price);
		 window.location.href = "#/page13";
	 };

//add to cart function
	 $scope.addToCart=function(id,image,name,price){ 
	 	var alertPopup = $ionicPopup.alert({
            title: 'Carrinho',
            template: 'item adicionado ao carrinho!'
        });
		cart.add(id, image, name, price, 1);	
	 };	 
	$scope.carrefour = function(){
		window.location.href ="#/page17";
	};
})  //--------------------frist-------------------//


.controller('marketDbCtrl', function($scope,$http,$rootScope,$ionicPopup,sharedCartService,sharedFilterService) {
	var cart = sharedCartService.cart;

	$scope.slide_items=[    			
		{"p_image_id":"slide_1"},				
		{"p_image_id":"slide_2"},
		{"p_image_id":"slide_3"}  
	];					   
	$scope.noMoreItemsAvailable = false; // lazy load list

	$scope.$on('$stateChangeSuccess', function() {
		$scope.loadMore();  //Added Infine Scroll
	});
	
	$scope.loadMore = function() {
			$scope.loading = true;
			str = sharedFilterService.getUrl(2);
			$http.get(str).success(function (response){
				//.log(response['records']);
				$scope.loading = false;
				$scope.menu_items = response[0].records;
				$rootScope.allItens = response[0].records;
				$scope.hasmore=response[0].has_more;	//"has_more": 0	or number of items left
				$scope.$broadcast('scroll.infiniteScrollComplete');
			});	

		//more data can be loaded or not
		if ($scope.hasmore == 0 ) {
			 $scope.noMoreItemsAvailable = true;
		}
	};
 //show product page
	$scope.showProductInfo=function (id,desc,img,name,price) {	 
		 sessionStorage.setItem('product_info_id', id);
		 sessionStorage.setItem('product_info_desc', desc);
		 sessionStorage.setItem('product_info_img', img);
		 sessionStorage.setItem('product_info_name', name);
		 sessionStorage.setItem('product_info_price', price);
		 window.location.href = "#/page13";
	 };

//add to cart function
	 $scope.addToCart=function(id,image,name,price){ 
	 	var alertPopup = $ionicPopup.alert({
            title: 'Carrinho',
            template: 'item adicionado ao carrinho!'
        });
		cart.add(id, image, name, price, 1);	
	 };	 


	$scope.carrefour = function(){
		window.location.href ="#/page17";
	};
})  //--------------------second-------------------//


.controller('marketNovaeraCtrl', function($scope,$http,$ionicPopup,$rootScope,sharedCartService,sharedFilterService) {
	var cart = sharedCartService.cart;

	$scope.slide_items=[    			
		{"p_image_id":"slide_1"},				
		{"p_image_id":"slide_2"},
		{"p_image_id":"slide_3"}  
	];					   
	$scope.noMoreItemsAvailable = false; // lazy load list

	$scope.$on('$stateChangeSuccess', function() {
		$scope.loadMore();  //Added Infine Scroll
	});
	
	$scope.loadMore = function() {
			$scope.loading = true;
			str = sharedFilterService.getUrl(3);
			$http.get(str).success(function (response){
				//.log(response['records']);
				$scope.loading = false;
				$scope.menu_items = response[0].records;
				$rootScope.allItens = response[0].records;
				$scope.hasmore=response[0].has_more;	//"has_more": 0	or number of items left
				$scope.$broadcast('scroll.infiniteScrollComplete');
			});	
 //more data can be loaded or not
		if ($scope.hasmore == 0 ) {
			 $scope.noMoreItemsAvailable = true;
		}
	};
 //show product page
	$scope.showProductInfo=function (id,desc,img,name,price) {	 
		 sessionStorage.setItem('product_info_id', id);
		 sessionStorage.setItem('product_info_desc', desc);
		 sessionStorage.setItem('product_info_img', img);
		 sessionStorage.setItem('product_info_name', name);
		 sessionStorage.setItem('product_info_price', price);
		 window.location.href = "#/page13";
	 };

 //add to cart function
	 $scope.addToCart=function(id,image,name,price){ 
	 	var alertPopup = $ionicPopup.alert({
            title: 'Carrinho',
            template: 'item adicionado ao carrinho!'
        });
		cart.add(id, image, name, price, 1);	
	 };	 


	$scope.carrefour = function(){
		window.location.href ="#/page17";
	};
})

.controller('filterByCarrefourCtrl', function($scope,sharedFilterService) {

  $scope.Categories = [
    {id: 1, name: 'Material de limpeza'},
	{id: 2, name: 'Alimentícios'},
	{id: 3, name: 'Bebidas'},
	{id: 4, name: 'Higiene Pessoal'}
  ];
  
  $scope.getCategory = function(cat_list){
    categoryAdded = cat_list;
	var c_string = ""; 	// will hold the category as string
	
	for(var i=0; i < categoryAdded.length; i++)
		 c_string += (categoryAdded[i].id+"||"); 
	
	c_string = c_string.substr(0, c_string.length - 2);
	sharedFilterService.category = c_string;
	window.location.href = "#/page17";
  };

})

  .controller('filterByDBsuperCtrl', function($scope,sharedFilterService) {

  $scope.Categories = [
    {id: 1, name: 'Material de limpeza'},
	{id: 2, name: 'Alimentícios'},
	{id: 3, name: 'Bebidas'},
	{id: 4, name: 'Higiene Pessoal'}
  ];
  
  $scope.getCategory = function(cat_list){
    categoryAdded = cat_list;
	var c_string = ""; 	// will hold the category as string
	
	for(var i=0; i < categoryAdded.length; i++)
		 c_string += (categoryAdded[i].id+"||"); 
	
	c_string = c_string.substr(0, c_string.length - 2);
	sharedFilterService.category = c_string;
	window.location.href = "#/page18";
  };

})

.controller('filterByNovaEraCtrl', function($scope,sharedFilterService) {

  $scope.Categories = [
    {id: 1, name: 'Material de limpeza'},
	{id: 2, name: 'Alimentícios'},
	{id: 3, name: 'Bebidas'},
	{id: 4, name: 'Higiene Pessoal'}
  ];
  
  $scope.getCategory = function(cat_list){
    categoryAdded = cat_list;
	var c_string = ""; 	// will hold the category as string
	
	for(var i=0; i < categoryAdded.length; i++)
		 c_string += (categoryAdded[i].id+"||"); 
	
	c_string = c_string.substr(0, c_string.length - 2);
	sharedFilterService.category = c_string;
	window.location.href = "#/page19";
  };

})  //Final Controller Market -----------------------------------------------------------------//


.controller('productPageCtrl', function($scope) {	//onload event

	angular.element(document).ready(function () {
		$scope.id= sessionStorage.getItem('product_info_id');
		$scope.desc= sessionStorage.getItem('product_info_desc');
		$scope.img= "img/"+ sessionStorage.getItem('product_info_img')+".jpg";
		$scope.name= sessionStorage.getItem('product_info_name');
		$scope.price= sessionStorage.getItem('product_info_price');
	});

})
